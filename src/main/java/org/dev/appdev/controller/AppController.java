package org.dev.appdev.controller;

import org.dev.appdev.entity.Work;
import org.dev.appdev.repository.WorkRepository;
import org.dev.appdev.service.WorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class AppController {

    @Autowired
    WorkRepository workRepository;

    @Autowired
    WorkService workService;

    // Get All Work
    @GetMapping("/list_all")
    public List<Work> getAllWork(){
        return workRepository.findAll();
    }

    @PostMapping("/add_work")
    public Work createWork(@Valid @RequestBody Work w){
        return workRepository.save(w);
    }

    /*
    @PutMappging("/work/{id}")
    public ResponseEntity<Work> updateWork(@PathVariable(value = "id") Integer wid,@Valid @RequestBody Work workDetails){

        return null;
    }
    */


}
