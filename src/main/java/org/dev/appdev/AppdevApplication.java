package org.dev.appdev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class AppdevApplication {

    public static void main(String[] args) {

        SpringApplication.run(AppdevApplication.class, args);

    }
}
