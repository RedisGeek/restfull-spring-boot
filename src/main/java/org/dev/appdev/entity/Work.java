package org.dev.appdev.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "work_info")
public class Work implements Serializable{

    @Id
    @GeneratedValue()
    @Column(name = "id")
    private int id;

    @Column(name = "work_name")
    private String work_name;

    @Column(name = "work_desc")
    private String work_desc;

    @Column(name = "work_nbr")
    private int work_nbr;

    @Column(name = "work_team")
    private String work_team;

    @Column(name = "work_team_lead")
    private String work_team_lead;

    @Column(name = "work_date")
    private Date work_date;

    @Column(name = "work_date_fin")
    private Date work_date_fin;

    @Column(name = "work_note")
    private String work_note;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWork_name() {
        return work_name;
    }

    public void setWork_name(String work_name) {
        this.work_name = work_name;
    }

    public String getWork_desc() {
        return work_desc;
    }

    public void setWork_desc(String work_desc) {
        this.work_desc = work_desc;
    }

    public int getWork_nbr() {
        return work_nbr;
    }

    public void setWork_nbr(int work_nbr) {
        this.work_nbr = work_nbr;
    }

    public String getWork_team() {
        return work_team;
    }

    public void setWork_team(String work_team) {
        this.work_team = work_team;
    }

    public String getWork_team_lead() {
        return work_team_lead;
    }

    public void setWork_team_lead(String work_team_lead) {
        this.work_team_lead = work_team_lead;
    }

    public Date getWork_date() {
        return work_date;
    }

    public void setWork_date(Date work_date) {
        this.work_date = work_date;
    }

    public Date getWork_date_fin() {
        return work_date_fin;
    }

    public void setWork_date_fin(Date work_date_fin) {
        this.work_date_fin = work_date_fin;
    }

    public String getWork_note() {
        return work_note;
    }

    public void setWork_note(String work_note) {
        this.work_note = work_note;
    }


    @Override
    public String toString() {
        return "Work{" +
                "id=" + id +
                ", work_name='" + work_name + '\'' +
                ", work_desc='" + work_desc + '\'' +
                ", work_nbr=" + work_nbr +
                ", work_team='" + work_team + '\'' +
                ", work_team_lead='" + work_team_lead + '\'' +
                ", work_date=" + work_date +
                ", work_date_fin=" + work_date_fin +
                ", work_note='" + work_note + '\'' +
                '}';
    }


    public Work() {

    }
}
