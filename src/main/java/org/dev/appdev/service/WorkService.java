package org.dev.appdev.service;

import org.dev.appdev.entity.Work;
import org.dev.appdev.repository.WorkRepository;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class WorkService implements WorkRepository {


    @Override
    public List<Work> findAll() {
        return null;
    }

    @Override
    public List<Work> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Work> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<Work> findAllById(Iterable<Integer> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Integer integer) {

    }

    @Override
    public void delete(Work work) {

    }

    @Override
    public void deleteAll(Iterable<? extends Work> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends Work> S save(S s) {
        return null;
    }

    @Override
    public <S extends Work> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<Work> findById(Integer integer) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Integer integer) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Work> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<Work> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Work getOne(Integer integer) {
        return null;
    }

    @Override
    public <S extends Work> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Work> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Work> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Work> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Work> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Work> boolean exists(Example<S> example) {
        return false;
    }
}
